package cn.dipper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WEBServer {

	/** 
	* @Title: serverStart 
	* @Description: 监听80端口
	* @param     设定文件 
	* @return void    返回类型 
	* @throws 
	*/
	public void serverStart(int port){
		try {
			System.out.println(port);
			ServerSocket serverSocket = new ServerSocket(port);
			//等待连接
			while(true){
				
				Socket socket = serverSocket.accept();
				//多线程处理，每个用户都新建一个处理对象
				new Processor(socket).start();
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		int port = 8080;
		
		if(args.length==1){
			port = Integer.parseInt(args[0]);
		}
		//生成实例，调用端口监听
		new WEBServer().serverStart(port);
	}

}
